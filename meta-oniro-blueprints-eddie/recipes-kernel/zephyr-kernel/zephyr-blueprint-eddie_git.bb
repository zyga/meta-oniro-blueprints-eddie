# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

require recipes-kernel/zephyr-kernel/zephyr-sample.inc

SUMMARY = "Eddie blueprint: Zephyr application"
DESCRIPTION = "Zephyr based Eddie blueprint image"
LICENSE = "Apache-2.0"

SRC_OPT_PROTO = "protocol=https"
SRC_OPT_DEST = "destsuffix=git/apps/eddie"
SRC_OPT_NAME = "name=eddie"
SRC_OPT_BRANCH = "branch=main"

SRC_OPTIONS = "${SRC_OPT_PROTO};${SRC_OPT_DEST};${SRC_OPT_NAME};${SRC_OPT_BRANCH}"
SRC_URI += "git://gitlab.eclipse.org/mrfrank/eddie-zephyr.git;${SRC_OPTIONS}"

SRCREV_eddie = "2a28abf6928f0aa76005335a378bbe695875dcdc"

ZEPHYR_SRC_DIR = "${S}/apps/eddie"


# The overlay config and OpenThread itself imposes some specific requirements
# towards the boards (e.g. flash layout and ieee802154 radio) so we need to
# limit to known working machines here.
COMPATIBLE_MACHINE = "arduino-nano-33-ble|qemu-x86"

EXTRA_OECMAKE:append:arduino-nano-33-ble = ' -DCONF_FILE="prj.conf overlay-openthread.conf" -DCONFIG_OPENTHREAD_THREAD_VERSION_1_2=y'
EXTRA_OECMAKE:append:qemu-x86 = ' -DCONF_FILE="prj.conf overlay-e1000.conf"'
